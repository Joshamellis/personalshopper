import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

public class AddForm {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	PersonalShopper personalShopperConnection = new PersonalShopper();
	
	//Sets visibility of this form
	public void SetVisible(boolean visible) {
		frame.setVisible(visible);
	}
	
	//Creates the application
	public AddForm() {
		initialize();
	}

	//Initialize the contents of the frame.
	private void initialize(){
		frame = new JFrame();
		frame.setBounds(100, 100, 320, 215);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(158, 11, 136, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(158, 42, 136, 20);
		frame.getContentPane().add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(158, 73, 136, 20);
		frame.getContentPane().add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(158, 104, 136, 20);
		frame.getContentPane().add(textField_3);

		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(168, 135, 126, 29);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//If data is in the correct format, add to csvDatabase var and update the file
				if (personalShopperConnection.IsNumeric(textField_1.getText()) && personalShopperConnection.IsNumeric(textField_2.getText()) && personalShopperConnection.IsNumeric(textField_3.getText())){
					PersonalShopper.csvDatabase[CSVConnection.count][0] = textField.getText();
					PersonalShopper.csvDatabase[CSVConnection.count][1] = PersonalShopper.category;
					PersonalShopper.csvDatabase[CSVConnection.count][2] = textField_1.getText();
					PersonalShopper.csvDatabase[CSVConnection.count][3] = textField_2.getText();
					PersonalShopper.csvDatabase[CSVConnection.count][4] = textField_3.getText();
					try {
						CSVConnection.Update(1);
						frame.setVisible(false);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		frame.getContentPane().add(btnAdd);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(10, 135, 126, 29);
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Hide form
				frame.setVisible(false);
			}
		});
		frame.getContentPane().add(btnCancel);

		JLabel lblProductName = new JLabel("Product Name");
		lblProductName.setBounds(69, 14, 67, 14);
		frame.getContentPane().add(lblProductName);

		JLabel lblMinimumAmountRequired = new JLabel("Minimum amount Required");
		lblMinimumAmountRequired.setBounds(10, 45, 126, 14);
		frame.getContentPane().add(lblMinimumAmountRequired);

		JLabel lblMaximumAmountRequired = new JLabel("Maximum amount Required");
		lblMaximumAmountRequired.setBounds(10, 76, 138, 14);
		frame.getContentPane().add(lblMaximumAmountRequired);

		JLabel lblCurrentQuantityOwned = new JLabel("Current Quantity Owned");
		lblCurrentQuantityOwned.setBounds(22, 107, 126, 14);
		frame.getContentPane().add(lblCurrentQuantityOwned);
	}
}
