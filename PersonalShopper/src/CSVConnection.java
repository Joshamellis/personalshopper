import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CSVConnection {
	static String csvFile = System.getProperty("user.dir") + "/src/lib/file.csv";
	static int count = 0;
	PersonalShopper personalShopperConnection = new PersonalShopper();

	static public void Refresh(){
		count = 0;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		//Loads data in file to array in the correct format (CSV)
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] rawData = line.split(cvsSplitBy);
				for(int i=0; i<5; i++){
					PersonalShopper.csvDatabase[count][i] = rawData[i];
				}
				count++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	static public void Update(int num) throws IOException {
		BufferedWriter wipe = new BufferedWriter(new FileWriter(csvFile));
		//Wipes the file ready for appending
		wipe.write("");
		wipe.close();
		BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile, true));
		//Loops through csvDatabase and adds values to file in the correct format (CSV)
		for (int i=0; i<count + num; i++){
			for (int j=0; j<5; j++){
				if(PersonalShopper.csvDatabase[i][j] != null && !PersonalShopper.csvDatabase[i][j].equals("DEL")){
					writer.write(PersonalShopper.csvDatabase[i][j]);
					if (j!=4){
						writer.write(",");
					}else{
						writer.newLine();
					}
				}
			}
		}
		writer.close();
		//Refresh the csvDatabse with the new data in the file
		CSVConnection.Refresh();
	}
}