import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CategoryForm {

	private JFrame frame;
	private JTable table;
	boolean check;
	PersonalShopper personalShopperConnection = new PersonalShopper();

	//Create the application.
	public CategoryForm() {
		initialize();
	}

	//Sets visibility of the form
	public void SetVisible(boolean visible) {
		frame.setVisible(visible);
	}

	//Initialize the contents of the frame.
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 700);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		//Sets correct title of form
		JLabel lblTitle = new JLabel("#**#  #**#");
		switch (PersonalShopper.category){
		case "cb":
			lblTitle.setText("Cupboard Food");
			break;
		case "ff":
			lblTitle.setText("Fridge/Freezer Food");
			break;
		case "cl":
			lblTitle.setText("Cleaning Products");
			break;
		case "be":
			lblTitle.setText("Bathroom Essentials");
			break;
		case "":
			break;
		}
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblTitle.setBounds(104, 11, 270, 65);
		frame.getContentPane().add(lblTitle);

		JButton btnAdd = new JButton("Add Data");
		btnAdd.setBounds(10, 72, 110, 50);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Shows relevant form
				AddForm con = new AddForm();
				con.SetVisible(true);
			}
		});
		frame.getContentPane().add(btnAdd);

		JButton btnDel = new JButton("Delete Data");
		btnDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Shows relevant form and adds needed data as variables in PersonalShopper to be retrieved, if a cell is selected
				if (personalShopperConnection.IsCellSelected(table)){
					personalShopperConnection.FillValues(table.getModel().getValueAt(table.getSelectedRow(), 0).toString(), String.valueOf(table.getValueAt(table.getSelectedRow(), 0)), String.valueOf(table.getValueAt(table.getSelectedRow(), 1)), String.valueOf(table.getValueAt(table.getSelectedRow(), 2)), String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
				}
				DeleteForm con = new DeleteForm();
				con.SetVisible(true);
			}
		});
		btnDel.setBounds(133, 72, 110, 50);
		frame.getContentPane().add(btnDel);

		JButton btnEdit = new JButton("Edit Data");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Shows relevant form and adds needed data as variables in PersonalShopper to be retrieved, if a cell is selected
				if (personalShopperConnection.IsCellSelected(table)){
					personalShopperConnection.FillValues(table.getModel().getValueAt(table.getSelectedRow(), 0).toString(), String.valueOf(table.getValueAt(table.getSelectedRow(), 0)), String.valueOf(table.getValueAt(table.getSelectedRow(), 1)), String.valueOf(table.getValueAt(table.getSelectedRow(), 2)), String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
					EditForm con = new EditForm();
					con.SetVisible(true);
				}
			}
		});
		btnEdit.setBounds(264, 72, 110, 50);
		frame.getContentPane().add(btnEdit);

		table = new JTable(new DefaultTableModel(new Object[]{"Column1", "Column2", "Column3", "Column4"}, 0));
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		//Adds relevant data to the table
		for (int i=0; i<CSVConnection.count; i++){
			if (PersonalShopper.csvDatabase[i][1].equals(PersonalShopper.category)){
				model.addRow(new Object[]{PersonalShopper.csvDatabase[i][0], PersonalShopper.csvDatabase[i][2], PersonalShopper.csvDatabase[i][3], PersonalShopper.csvDatabase[i][4]});	
			}
		}
		table.setBounds(10, 133, 364, 483);
		frame.getContentPane().add(table);

		JButton btnDecrease = new JButton("Decrease Quantity");
		btnDecrease.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				//On button clicks
				//Decreases the value of the selected row in the "quantity" column by 1 
				if (personalShopperConnection.IsCellSelected(table)){
					personalShopperConnection.FillValues(table.getModel().getValueAt(table.getSelectedRow(), 0).toString(), String.valueOf(table.getValueAt(table.getSelectedRow(), 0)), String.valueOf(table.getValueAt(table.getSelectedRow(), 1)), String.valueOf(table.getValueAt(table.getSelectedRow(), 2)), String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
					personalShopperConnection.AlterQuantity(-1, table);	
				}
			}
		});
		btnDecrease.setBounds(10, 627, 177, 23);
		frame.getContentPane().add(btnDecrease);

		JButton btnIncrease = new JButton("Increase Quantity");
		btnIncrease.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				//On button clicks
				//Increases the value of the selected row in the "quantity" column by 1
				if (personalShopperConnection.IsCellSelected(table)){
					personalShopperConnection.FillValues(table.getModel().getValueAt(table.getSelectedRow(), 0).toString(), String.valueOf(table.getValueAt(table.getSelectedRow(), 0)), String.valueOf(table.getValueAt(table.getSelectedRow(), 1)), String.valueOf(table.getValueAt(table.getSelectedRow(), 2)), String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
					personalShopperConnection.AlterQuantity(1, table);
				}
			}
		});
		btnIncrease.setBounds(197, 627, 177, 23);
		frame.getContentPane().add(btnIncrease);

		JButton btnShowForm = new JButton("<--");
		btnShowForm.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnShowForm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				//On button clicks
				//loads home form
				frame.setVisible(false);
				HomeForm connection = new HomeForm();
				connection.frame.setVisible(true);
			}
		});
		btnShowForm.setBounds(10, 11, 89, 50);
		frame.getContentPane().add(btnShowForm);
	}
}
