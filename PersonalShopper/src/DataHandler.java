import javax.swing.JTable;

public class DataHandler {
	public int FindPositionInArray(JTable table){
		int row = 0;
		String curProduct = PersonalShopper.csvDatabase[row][0];
		//Finds the index of the value equal to that of the selected cell in the table
		while(!curProduct.equals(table.getModel().getValueAt(table.getSelectedRow(), 0))){
			row++;
			curProduct = PersonalShopper.csvDatabase[row][0];
		}
		return row;
	}
	
	public boolean IsNumeric(String value){
		int size = value.length();
		//Checks if 'value' is numeric
		for (int i=0; i<size; i++){
			if(!Character.isDigit(value.charAt(i))){
				return false;
			}
		}
		return size > 0;
	}
}
