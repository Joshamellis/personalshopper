import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

public class DeleteForm {

	private JFrame frame;
	PersonalShopper personalShopperConnection = new PersonalShopper(); 

	// Create the application.
	public DeleteForm() {
		initialize();
	}
	
	//Sets the visibility of the form
	public void SetVisible(boolean visible) {
		frame.setVisible(visible);
	}

	//Initialize the contents of the frame.
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 285, 108);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 249, 14);
		frame.getContentPane().add(lblNewLabel);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Deletes data
				personalShopperConnection.DeleteData();
				try {
					CSVConnection.Update(0);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(158, 36, 89, 23);
		frame.getContentPane().add(btnDelete);

		//Hides form
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(20, 36, 89, 23);
		frame.getContentPane().add(btnCancel);
	}

}
