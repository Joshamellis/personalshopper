import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class EditForm {

	private JFrame frame;
	private JTextField textField = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JTextField textField_2 = new JTextField();
	private JTextField textField_3 = new JTextField();
	private JComboBox<String> comboBox;
	PersonalShopper personalShopperConnection = new PersonalShopper();

	public void SetVisible(boolean visible) {
		frame.setVisible(visible);
	}

	// Create the application.
	public EditForm() {
		initialize();
		textField_3.setText(PersonalShopper.product);
		textField_2.setText(PersonalShopper.min);
		textField_1.setText(PersonalShopper.max);
		textField.setText(PersonalShopper.quantity);
	}

	// Initialize the contents of the frame.
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 320, 245);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Ammends data with values on the form
				personalShopperConnection.MakeAlterations(textField_3.getText(), personalShopperConnection.GetCategory(comboBox.getSelectedIndex()), textField_2.getText(), textField_1.getText(), textField.getText());
				frame.setVisible(false);
			}
		});
		btnEdit.setBounds(168, 166, 126, 29);
		frame.getContentPane().add(btnEdit);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button clicks
				//Hides form
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(10, 166, 126, 29);
		frame.getContentPane().add(btnCancel);

		JLabel label = new JLabel("Current Quantity Owned");
		label.setBounds(22, 138, 126, 14);
		frame.getContentPane().add(label);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(158, 135, 136, 20);
		frame.getContentPane().add(textField);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(158, 104, 136, 20);
		frame.getContentPane().add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(158, 73, 136, 20);
		frame.getContentPane().add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(158, 11, 136, 20);
		frame.getContentPane().add(textField_3);

		JLabel label_1 = new JLabel("Product Name");
		label_1.setBounds(69, 14, 67, 14);
		frame.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("Minimum amount Required");
		label_2.setBounds(10, 76, 126, 14);
		frame.getContentPane().add(label_2);

		JLabel label_3 = new JLabel("Maximum amount Required");
		label_3.setBounds(10, 107, 138, 14);
		frame.getContentPane().add(label_3);

		comboBox = new JComboBox<String>();
		comboBox.addItem("Cupboard Food");
		comboBox.addItem("Fridge/Freezer Food");
		comboBox.addItem("Cleaning Products");
		comboBox.addItem("Bathroom Essentials");
		comboBox.setSelectedIndex(personalShopperConnection.FindComboIndex());
		comboBox.setBounds(158, 42, 136, 20);
		frame.getContentPane().add(comboBox);

		JLabel lblCategory = new JLabel("Category");
		lblCategory.setBounds(86, 45, 50, 14);
		frame.getContentPane().add(lblCategory);
	}
}
