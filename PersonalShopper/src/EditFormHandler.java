import java.io.IOException;

public class EditFormHandler {
	
	public int FindComboIndex(){
		int index=0;
		//Selects the correct index for the category associated with the data
		switch (PersonalShopper.category){
		case "cb":
			index = 0;
			break;
		case "ff":
			index = 1;
			break;
		case "cl":
			index = 2;
			break;
		case "be":
			index = 3;
			break;
		case "":
			break;
		}
		return index;
	}
	
	public void MakeAlterations(int row, String newProduct, String newCategory, String newMin, String newMax, String newQuantity){
		//Alters data with values given
		PersonalShopper.csvDatabase[row][0] = newProduct;
		PersonalShopper.csvDatabase[row][1] = newCategory;
		PersonalShopper.csvDatabase[row][2] = newMin;
		PersonalShopper.csvDatabase[row][3] = newMax;
		PersonalShopper.csvDatabase[row][4] = newQuantity;
		try {
			CSVConnection.Update(0);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public String GetCategory(int index){
		//Finds the category associated with the index of the comboBox selected item
		String category = "";
		switch (index){
		case 0:
			category = "cb";
			break;
		case 1:
			category = "ff";
			break;
		case 2:
			category = "cl";
			break;
		case 3:
			category = "be";
			break;
		}
		return category;
	}
}