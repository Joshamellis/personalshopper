import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomeForm {

	JFrame frame;
	PersonalShopper personalShopperConnection = new PersonalShopper();

	//Create application
	public HomeForm() {
		initialize();
	}
	
	//Sets visibility of the form
	public void SetVisible(boolean visible) {
		frame.setVisible(visible);
	}

	//Initialise JFrame
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnCupboard = new JButton("Cupboard Food");
		btnCupboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button click
				//Loads the form, hides this form and sets category so the correct data shows
				PersonalShopper.category = "cb";
				frame.setVisible(false);
				CategoryForm connection = new CategoryForm();
				connection.SetVisible(true);
			}
		});
		btnCupboard.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnCupboard.setBounds(10, 11, 364, 120);
		frame.getContentPane().add(btnCupboard);
		
		JButton btnFridgeFreezer = new JButton("Fridge/ Freezer Food");
		btnFridgeFreezer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button click
				//Loads the form, hides this form and sets category so the correct data shows
				PersonalShopper.category = "ff";
				frame.setVisible(false);
				CategoryForm connection = new CategoryForm();
				connection.SetVisible(true);
			}
		});
		btnFridgeFreezer.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnFridgeFreezer.setBounds(10, 142, 364, 120);
		frame.getContentPane().add(btnFridgeFreezer);
		
		JButton btnCleaning = new JButton("Cleaning Products");
		btnCleaning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button click
				//Loads the form, hides this form and sets category so the correct data shows
				PersonalShopper.category = "cl";
				frame.setVisible(false);
				CategoryForm connection = new CategoryForm();
				connection.SetVisible(true);
			}
		});
		btnCleaning.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnCleaning.setBounds(10, 273, 364, 120);
		frame.getContentPane().add(btnCleaning);
		
		JButton btnBathroom = new JButton("Bathroom Essentials");
		btnBathroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button click
				//Loads the form, hides this form and sets category so the correct data shows
				PersonalShopper.category = "be";
				frame.setVisible(false);
				CategoryForm connection = new CategoryForm();
				connection.SetVisible(true);
			}
		});
		btnBathroom.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnBathroom.setBounds(10, 404, 364, 120);
		frame.getContentPane().add(btnBathroom);
		
		JButton btnShoppingList = new JButton("Generate Shopping List");
		btnShoppingList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//On button click
				//Loads the form
				ShoppingForm connection = new ShoppingForm();
				connection.SetVisible(true);
			}
		});
		btnShoppingList.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnShoppingList.setBounds(10, 590, 364, 60);
		frame.getContentPane().add(btnShoppingList);
	}
}
