import java.awt.EventQueue;
import javax.swing.JTable;

public class PersonalShopper {
	static String category = "";
	static String[][] csvDatabase = new String[30][5];
	static String[] selectedValues = new String[4];
	static String product;
	static String min;
	static String max;
	static String quantity;
	static String selectedProduct;
	
	TableHandler tableHandlerConnection = new TableHandler();
	EditFormHandler editFormHandlerConnection = new EditFormHandler();
	DeleteFormHandler deleteFormHandlerConnection = new DeleteFormHandler();
	DataHandler dataHandlerConnection = new DataHandler();
	
	//Launch the application
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeForm window = new HomeForm();
					window.frame.setVisible(true);
					CSVConnection.Refresh();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	//Runs the method with the given values
	public boolean IsCellSelected(JTable table) {
		return tableHandlerConnection.IsCellSelected(table);
	}

	//Runs the method with the given values
	public void AlterQuantity(int quantifier, JTable table) {
		tableHandlerConnection.AlterQuantity(FindPositioninDatabase(selectedProduct), quantifier, table);
	}
	
	//Runs the method with the given values
	public void FillValues(String oldProduct, String newProduct, String newMin, String newMax, String newQuantity){
		selectedProduct = oldProduct;
		product = newProduct;
		min = newMin;
		max = newMax;
		quantity = newQuantity;
	}
	
	//Runs the method with the given values
	public void MakeAlterations(String newProduct, String newCategory, String newMin, String newMax, String newQuantity){
		editFormHandlerConnection.MakeAlterations(FindPositioninDatabase(selectedProduct), newProduct, newCategory, newMin, newMax, newQuantity);
	}
	
	//Runs the method with the given values
	public int FindPositioninDatabase(String product){
		return tableHandlerConnection.FindPositioninDatabase(product);
	}
	
	//Runs the method with the given values
	public String GetCategory(int index){
		return editFormHandlerConnection.GetCategory(index);
	}
	
	//Runs the method with the given values
	public int FindComboIndex(){
		return editFormHandlerConnection.FindComboIndex();
	}
	
	//Runs the method with the given values
	public void DeleteData(){
		deleteFormHandlerConnection.DeleteData(FindPositioninDatabase(selectedProduct));
	}
	
	//Runs the method with the given values
	public boolean IsNumeric(String value){
		return dataHandlerConnection.IsNumeric(value);
	}
}
