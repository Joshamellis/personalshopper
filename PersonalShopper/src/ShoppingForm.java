import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ShoppingForm {

	public JFrame frame;
	static JTable table;
	PersonalShopper personalShopperConnection = new PersonalShopper();

	//Create the application.
	public ShoppingForm() {
		initialize();
	}

	//Sets visibility of the form
	public void SetVisible(boolean visible) {
		frame.setVisible(visible);
	}
	
	//Initialize the contents of the frame.
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 700);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Shopping List");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 40));
		lblNewLabel.setBounds(120, 11, 243, 65);
		frame.getContentPane().add(lblNewLabel);

		table = new JTable(new DefaultTableModel(new Object[]{"Column1", "Column2", "Column3", "Column4"}, 0));
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		//Fills the table with the correct data (any products that have a lower quantity than minimum threshold)
		for (int i=0; i<CSVConnection.count; i++){
			if (Integer.parseInt(PersonalShopper.csvDatabase[i][4]) < Integer.parseInt(PersonalShopper.csvDatabase[i][2])){
				model.addRow(new Object[]{PersonalShopper.csvDatabase[i][0], PersonalShopper.csvDatabase[i][2], PersonalShopper.csvDatabase[i][3], PersonalShopper.csvDatabase[i][4]});	
			} 
		}
		table.setBounds(10, 87, 364, 529);
		frame.getContentPane().add(table);

		JButton btnDecrease = new JButton("Decrease Quantity");
		btnDecrease.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				//On button clicks
				//Decreases the value of the selected row in the "quantity" column by 1
				if (personalShopperConnection.IsCellSelected(table)){
					personalShopperConnection.FillValues(table.getModel().getValueAt(table.getSelectedRow(), 0).toString(), String.valueOf(table.getValueAt(table.getSelectedRow(), 0)), String.valueOf(table.getValueAt(table.getSelectedRow(), 1)), String.valueOf(table.getValueAt(table.getSelectedRow(), 2)), String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
					personalShopperConnection.AlterQuantity(-1, table);	
				}
			}
		});
		btnDecrease.setBounds(10, 627, 177, 23);
		frame.getContentPane().add(btnDecrease);

		JButton btnIncrease = new JButton("Increase Quantity");
		btnIncrease.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				//On button clicks
				//Increases the value of the selected row in the "quantity" column by 1
				if (personalShopperConnection.IsCellSelected(table)){
					personalShopperConnection.FillValues(table.getModel().getValueAt(table.getSelectedRow(), 0).toString(), String.valueOf(table.getValueAt(table.getSelectedRow(), 0)), String.valueOf(table.getValueAt(table.getSelectedRow(), 1)), String.valueOf(table.getValueAt(table.getSelectedRow(), 2)), String.valueOf(table.getValueAt(table.getSelectedRow(), 3)));
					personalShopperConnection.AlterQuantity(1, table);
				}
			}
		});
		btnIncrease.setBounds(197, 627, 177, 23);
		frame.getContentPane().add(btnIncrease);
		
		JButton btnShowForm = new JButton("<--");
		btnShowForm.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnShowForm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				//On button clicks
				//Hides this form and shows the home form
				frame.setVisible(false);
				HomeForm connection = new HomeForm();
				connection.frame.setVisible(true);
			}
		});
		btnShowForm.setBounds(10, 11, 89, 50);
		frame.getContentPane().add(btnShowForm);
	}
}
