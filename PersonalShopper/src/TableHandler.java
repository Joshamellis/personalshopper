import java.io.IOException;

import javax.swing.JTable;

public class TableHandler {
	public boolean IsCellSelected(JTable table){
		//Loops through each cell in the table and returns true if a selected cell is found
		for (int i=0; i<table.getRowCount(); i++){
			for (int j=0; j<table.getColumnCount(); j++){
				if (table.isCellSelected(i, j)){
					return true;
				}
			}
		}
		return false;
	}
	
	public void AlterQuantity(int row, int quantifier, JTable table){
		//Alters the correct value with the quantifier given and updates the csv file
		if ((Integer.parseInt(String.valueOf(table.getModel().getValueAt(table.getSelectedRow(), 3))) > 0 && quantifier < 0) || (quantifier > 0)){
			table.getModel().setValueAt(Integer.parseInt(String.valueOf(table.getModel().getValueAt(table.getSelectedRow(), 3))) + quantifier, table.getSelectedRow(), 3);
			PersonalShopper.csvDatabase[row][4] = String.valueOf(table.getModel().getValueAt(table.getSelectedRow(), 3));
			try {
				CSVConnection.Update(0);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public int FindPositioninDatabase(String product){
		String curProduct = PersonalShopper.csvDatabase[0][0];
		int row = 0;
		//Loops through the array until the correct value is found, then outputting the index of the value in the array
		while(!curProduct.equals(product)){
			row++;
			curProduct = PersonalShopper.csvDatabase[row][0];
		}
		return row;
	}
}
